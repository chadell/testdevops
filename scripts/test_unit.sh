#!/bin/bash

set -ex

pip install -r app/app_requirements.txt

coverage run app/test_app.py

coverage report app/app.py

